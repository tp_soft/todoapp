<?php
/**
 * Created by PhpStorm.
 * User: omitobisam
 * Date: 8.06.16
 * Time: 15:16
 */

require_once '../app/init.php';

if(isset($_POST['name'])){
    $name = trim($_POST['name']);

    echo $_POST['name'],' is the name';
    if(!empty($name)){
        $addedQuery = $db->prepare("
           insert into items (name, user, done, created)
           values (:name, :user, 0, now())
        ");

        $addedQuery->execute([
            'name' => $name,
            'user' => $_SESSION['user_id']
        ]);
    }
} else{
    die('this is the other way out!');
}
header('Location: ../index.php');