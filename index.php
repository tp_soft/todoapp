<?php
/**
 * Created by PhpStorm.
 * User: omitobisam
 * Date: 8.06.16
 * Time: 14:06
 */
require_once 'app/init.php';


if(isset($_POST['logout'])){
    $_GET['action'] ='logout';
    $action = $_GET['action'];

    switch ($action) {
        case 'logout':
            $_GET['logstat']=0;
            session_destroy();
            header('Location: index.php?action=login');
            break;
    }
}


$itemsQuery = $db->prepare(
    "
    select id, name, done
    from items
    where user = :user
    "
);

$itemsQuery->execute([
    'user' => $_SESSION['user_id']
]);

$items = $itemsQuery->rowCount() ? $itemsQuery : [];

//echo "<pre>", print_r($items, true), "</pre>";

//foreach ($items as $item){
//    echo $item['name'], '<br>';
//}
?>

<!doctype html>
<html lang="en" xmlns="http://www.w3.org/1999/html">
    <head>
        <meta charset="UTF-8">
        <title>To do App</title>
        <link href="http://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">
        <link href="http://fonts.googleapis.com/css?family=Shadows+Into+Light+Two" rel="stylesheet">
        <link rel="stylesheet" href="styles/main.css">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
        <meta name="viewport"
              content="width=device-width, initial-scale=1.0">
    </head>
    <body>

        <?php if (!$_GET['action']=='login'): ?>
        <div class="list">
            <h1 class="header"> To do.</h1>
            <div class="items">
                <form action='' method="post">
                    <input type="submit" value="Logout" class="btn btn-danger" name="logout">
                </form>

            </div>
        <?php if(!empty($items)):?>
            <ul class="items">
                <?php foreach ($items as $item): ?>
                    <li>
                <span class="item<?php echo $item['done'] ? ' done' : ''; ?>">
                    <?php echo $item['name']; ?></span>
                        <?php if (!$item['done']): ?>
                            <a href="controllers/mark.php?as=done&item=<?php echo $item[id]?>" class="done-button">Mark as done</a>
                        <?php else: ?>
                            <a href="controllers/mark.php?as=undone&item=<?php echo $item[id]?>" class="done-button">Mark as Undone</a>
                        <?php endif; ?>
                    </li>
                <?php endforeach; ?>
                <!--           <li>
                               <span>Learn php</span>
                           </li>-->
            </ul>
        <?php else: ?>
            <p>You havent added any Item(s) yet.</p>
        <?php endif;?>

            <form class="item-add" action="controllers/add.php" method="post">
                <input type="text" name="name" placeholder="Type a new Item here." class="input" autocomplete="off" required>
                <input type="submit" value="Add" class="submit">
            </form>
        </div>

        <?php elseif($_GET['action']=='signup'): ?>
        <div class="list">
            <h1 class="header"> Sign up.</h1>
            <form class="item-add" action="services/regservice.php" method="post">
                <input type="text" name="fullname" placeholder="Full name." class="input" autocomplete="off" required>
                <input type="text" name="reguname" placeholder="Username." class="input" autocomplete="off" required>
                <input type="password" name="regpassword" placeholder="Password." class="input" required>
                <input type="submit" value="Signup" name="signup" class="btn btn-info"> <span class="item">Just Login<a href="index.php?action=login" class="btn btn-link">Here</a></span><br>
                <?php echo $_GET['message']; ?>
            </form>
        </div>
        <?php else: ?>
        <div class="list">
            <h1 class="header"> Log in.</h1>
            <form class="item-add" action="services/authservice.php" method="post">
                <input type="text" name="uname" placeholder="Username." class="input" autocomplete="off" required>
                <input type="password" name="password" placeholder="Password." class="input" autocomplete="off" required>
                <input type="submit" value="Login" name="login" class="btn btn-primary"> <span class="item">Need to register?<a href="index.php?action=signup" class="btn btn-link">Here</a></span><br>
                <?php echo '<span style=\'color: red\'>'.$_GET['message'].'</span>';?>
            </form>
        </div>
        <?php endif; ?>

    </body>
</html>
