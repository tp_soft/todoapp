<?php
/**
 * Created by PhpStorm.
 * User: omitobisam
 * Date: 9.06.16
 * Time: 16:17
 */


require_once '../app/init.php';
if(isset($_POST['signup'])){
    $fullname = trim($_POST['fullname']);
    $reguname = trim($_POST['reguname']);
    $regpassword = $_POST['regpassword'];

    if(!empty($fullname) && !empty($regpassword) && !empty($reguname)){
        $addedQuery = $db->prepare("
           insert into users (username, fullname, secretkey, created)
           values (:uname, :fullname, :password, now())
        ");

        $result = $addedQuery->execute([
            'uname' => $reguname,
            'password' => $regpassword,
            'fullname' => $fullname
        ]);

        if($result){
            $msg=$_POST['message'] ='Registration successful. You may now Login!';
            header('Location: ../index.php?message='.$msg);
        } else{
            $_GET['message'] ='Error somewhere!';
            die('Error Somehwere here...');
        }
    } else {
        $msg=$_POST['message'] ='Details likely empty! :)';
        header('Location: ../index.php?action=signup&message='.$msg);
        //die('Details likely empty! :)');
    }

} else{
    $_GET['message'] ='Error somewhere!';
    die('Error Somewhere here...');
    //die('Seems some values with discrepancy!');
}