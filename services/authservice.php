<?php
/**
 * Created by PhpStorm.
 * User: omitobisam
 * Date: 9.06.16
 * Time: 12:42
 */

require_once '../app/init.php';


if(isset($_POST['login'])){
    $uname = $_POST['uname'];
    $password = $_POST['password'];

    if(!empty($uname) && !empty($password)){
        $addedQuery = $db->prepare("
           select id from users
           where username = :uname
           and secretkey = :password
        ");

        $addedQuery->execute([
            'uname' => $uname,
            'password'=> $password
        ]);

        if($addedQuery->rowCount()>0){
            $_SESSION['user_id'] = $addedQuery->fetchColumn(0);
            header('Location: ../index.php');
        } else{
//            die("$uname not found!");
            $msg=$_GET['message'] ='Something wrong with credentials';
            header('Location: ../index.php?action=login&message='.$msg);
        }

    }
}

